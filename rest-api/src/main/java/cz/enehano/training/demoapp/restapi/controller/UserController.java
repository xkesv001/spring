package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;

import java.util.List;

public class UserController {

    public List<UserDto> getAllUsers() {
        throw new UnsupportedOperationException("Method " + this.getClass().getEnclosingMethod().getName() + " is not implemented yet.");
    }

    public UserDto getUser(Long id) {
        throw new UnsupportedOperationException("Method " + this.getClass().getEnclosingMethod().getName() + " is not implemented yet.");
    }

    public UserDto createUser(UserDto dto) {
        throw new UnsupportedOperationException("Method " + this.getClass().getEnclosingMethod().getName() + " is not implemented yet.");
    }

    public UserDto updateUser(UserDto dto, ong id) {
        throw new UnsupportedOperationException("Method " + this.getClass().getEnclosingMethod().getName() + " is not implemented yet.");
    }

    public void deleteUser(Long id) {
        throw new UnsupportedOperationException("Method " + this.getClass().getEnclosingMethod().getName() + " is not implemented yet.");
    }


}
